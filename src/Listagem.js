import React, { Component } from 'react'

import Conecta from './Conecta'
import ItemLista from './ItemLista'
import './listagem.css' 

export default class Listagem extends Component {
 
  state = {
    tabelas: []
   
  }
  async componentDidMount() {
    const lista = await Conecta.get('/tabelas')
    lista.data.sort(function(a, b) {return a.linhas_id < b.linhas_id ? -1 : 1})
    this.setState({tabelas: lista.data})
    
  }

  alterar = async(id) => {
    let novoPrefixo = prompt('Alterar ou incluir veículo')

    let alterado = await Conecta.put(`/tabelas/${id}`, {prefixo: novoPrefixo})

    let retorno = alterado.data[0]

    const lista = await Conecta.get('/tabelas')
    lista.data.sort(function(a, b) {return a.linhas_id < b.linhas_id ? -1 : 1})
    this.setState({tabelas: lista.data})
  }


 

 
  render() {
    return (
      <div className="container mt-2">
        <table className="table table-sm table-striped table-bordered table-action">
          <thead>
            <tr>
              <th>Cód. Linha: </th>
              <th>Sentido: </th>
              <th>Cód. tipo de dias: </th>
              <th>Horário: </th>
              <th>Prefixo: </th>
              <th>Adicionar Veículo: </th>
            </tr>
          </thead>
          <tbody>
            {this.state.tabelas.map(tabela => (
              <ItemLista key={tabela.id}
      id={tabela.id}
      linhas_id={tabela.linha}
      sentido={tabela.sentido}
      tipo_id={tabela.dia}
      horario={tabela.horario}
      prefixo={tabela.prefixo}
      alterar={this.alterar}
                                                  />
            ))}
          </tbody>
        </table>        
      </div>
    )
  }
}
