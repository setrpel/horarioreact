import React from 'react'


const ItemLista = props => {
  return (
    <tr>
      <td>{props.linhas_id}</td>
      <td>{props.sentido}</td>
      <td>{props.tipo_id}</td>
      <td>{props.horario}</td>
      <td>{props.prefixo}</td>
        <td><button className="btn btn-sm btn-warning mr-1" onClick={(e) => props.alterar(props.id, e)}>
          <i className="far fa-edit"></i> Adicionar
        </button></td>
    </tr>
  )
}

export default ItemLista