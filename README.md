This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## HoráriosReact
Programa com a finalidade de inserir veículos na tabela de horários do api horários PHP Laravel

## SRC/
Diretório onde publicamos todos os arquivos de programaçao referente ao desenvolvimento do projeto, junto com o Public

**App** tem a função de mapear as rotas

```
function App() {
  return (
    <Router>
      <Header />
      <Route path='/' exact component={Home} />
      <Route path='/listar' component={Listagem} />
      <Route path='/pesquisar' component={FormPesquisar} />
      </Router>
  );
```
**Conecta** tem a função de importar a biblioteca axios e realizar a conexão com a API

```
import axios from 'axios'
const Conecta = axios.create({baseURL: ' http://localhost:8000/api'})
// http://localhost:8000/api  https://sdhorario.000webhostapp.com/api
export default Conecta
```

**Header** é o cabeçalho do programa onde temos normalmente as informações globais da página

```
export default class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-md bg-primary navbar-dark mb-2 sticky-top">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img src="logo.png" alt="Logo" width="25"/> Horários
          </Link>

          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="collapsibleNavbar">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/listar">Listar</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/pesquisar">Pesquisar</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
```

**Home** Página incial

```
import React, { Component } from 'react'

export default class Home extends Component {
  render() {
    return (
      <div>
        <h2>Horários</h2>
      </div>
    )
  }
}
```

**Index** Inicia o aplicativo

```
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
```
**ItemLista** tem a finalidade de guardar as informações em lista para criar as alterações,

```
const ItemLista = props => {
  return (
    <tr>
      <td>{props.linhas_id}</td>
      <td>{props.sentido}</td>
      <td>{props.tipo_id}</td>
      <td>{props.horario}</td>
      <td>{props.prefixo}</td>
        <td><button className="btn btn-sm btn-warning mr-1" onClick={(e) => props.alterar(props.id, e)}>
          <i className="far fa-edit"></i> Adicionar
        </button></td>
    </tr>
  )
}
```

**Listagem** principal class para o sistema, nela iremos realizar as principais funções

```
export default class Listagem extends Component {
 
 // o state para criar a lista de dados que iremos consumir do api
  state = {
    tabelas: []
   
  }

  // faz a primeira consulta na api /tabelas para apresentarmos
  async componentDidMount() {
    const lista = await Conecta.get('/tabelas')
    lista.data.sort(function(a, b) {return a.linhas_id < b.linhas_id ? -1 : 1})
    this.setState({tabelas: lista.data})
    
  }

  //função para realizar a inserção ou alteração do veículo na tabela do sistema horários
  alterar = async(id) => {
    let novoPrefixo = prompt('Alterar ou incluir veículo')

    let alterado = await Conecta.put(`/tabelas/${id}`, {prefixo: novoPrefixo})

    let retorno = alterado.data[0]

    const lista = await Conecta.get('/tabelas')
    lista.data.sort(function(a, b) {return a.linhas_id < b.linhas_id ? -1 : 1})
    this.setState({tabelas: lista.data})
  }

//tabela de apresentação da estrutura dos dados que devemos visualizar consumidos do api 
  render() {
    return (
      <div className="container mt-2">
        <table className="table table-sm table-striped table-bordered table-action">
          <thead>
            <tr>
              <th>Cód. Linha: </th>
              <th>Sentido: </th>
              <th>Cód. tipo de dias: </th>
              <th>Horário: </th>
              <th>Prefixo: </th>
              <th>Adicionar Veículo: </th>
            </tr>
          </thead>
          <tbody>
            {this.state.tabelas.map(tabela => (
              <ItemLista key={tabela.id}
      id={tabela.id}
      linhas_id={tabela.linha}
      sentido={tabela.sentido}
      tipo_id={tabela.dia}
      horario={tabela.horario}
      prefixo={tabela.prefixo}
      alterar={this.alterar}
                                                  />
            ))}
          </tbody>
        </table>        
      </div>
    )
  }
}
```




  
